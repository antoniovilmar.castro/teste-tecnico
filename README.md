# Pré requisito

- Instalar o docker
- Instalar o git
- Criar conta no gitlab.com
- Criar conta no dockerhub
- Clonar o projeto utilizando SSH no git através do [link](https://gitlab.com/antoniovilmar.castro/teste-tecnico)

# Desafio

> Objetivo:
Conteinerizar o projeto utilizando a imagem ```ubuntu:latest```

O Dockerfile do projeto deve ter:

1. Executar o update no apt
2. Instalar o node na versão 16
3. Rodar o comando ```node init.js```
4. Publicar a imagem no dockerhub

Dica: Para testar a execução da imagem utilize o ```docker run```, se estiver correto você deve visualizar a mensagem "Aplicação Rodandoooo Parabéns!"